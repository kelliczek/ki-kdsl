# Julia Macro

## Setup

1. Install Julia (see https://julialang.org/downloads/)
1. For vs code/codium users
	```sh
	ext install julialang.language-julia
	```
1. Install MacroTools extention (see https://github.com/FluxML/MacroTools.jl)
	```
	] add MacroTools
	```


## Run

```sh
Julia: Execute active File in REPL
```

<br>

<hr>

<br>



# Parsy DSL

## Setup

Install parsy
```sh
pip3 install parsy
```

## Run

```sh
python3 graph.py
```