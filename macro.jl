using MacroTools: postwalk
using Printf

function ignored_key_words(sym::Symbol)
  return sym ∈ (:*, :+, :/, :-, :quote, :begin, :end, :if, :else, :elseif, :while, :for, :function, :macro, :return, :try, :catch, :finally, :do, :using, :import, :export, :const, :global, :local)
end

macro make_magic(paramms)
  string = postwalk(paramms) do ex
    if isa(ex, Symbol) && !ignored_key_words(ex)
      quote
        @show $ex # print expresion
        $ex
      end
    else
      ex
    end
  end

  return esc(string)
end


# run time
covid_confirmed_cases = 4636282
total_deaths = 42702
total_population = 10533399


alive_czechs = @make_magic((total_population - total_deaths))
percentage_ok_czechs = @make_magic(((total_deaths + covid_confirmed_cases) / total_population))

@printf "%s alive czechs | which is %s of total population" alive_czechs percentage_ok_czechs