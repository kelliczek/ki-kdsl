import json
from parsy import string, regex, seq, Parser

whitespace = regex(r'\s*')
left_brace = string('{') << whitespace
right_brace = string('}') << whitespace
colon = string(':') << whitespace
comma = string(',') << whitespace
number = regex(r'-?(?:0|[1-9]\d*)(?:\.\d+)?(?:[eE][+\-]?\d+)?').map(float)
string_literal = regex(r'"(?:[^"\\]|\\.)*"')
true_literal = string("true").result(True)
false_literal = string("false").result(False)
null_literal = string("null").result(None)

json_value = string_literal | number | true_literal | false_literal | null_literal

json_obj = Parser(lambda s: json_value.parse_partial(s).map(lambda tup: (tup[0], s[tup[1]:])))

pair = seq(string_literal << whitespace, colon, json_value).combine(lambda key, _, value: (key, value))
json_dict = seq(left_brace, pair.sep_by(comma).map(dict), right_brace)

identifier = regex(r'\$[a-zA-Z0-9_]+')
assignment = string('=')
obj_declaration = seq(identifier << whitespace, assignment << whitespace, json_dict)
declarations_parser = whitespace >> obj_declaration.sep_by(whitespace).many()

def parse_obj_declaration(text):
    return declarations_parser.parse(text)

def parse_json_with_references(json_string, declarations):
    json_with_refs = regex.compile(r'\$[a-zA-Z0-9_]+')

    def replace_reference(match):
        ref = match.group(0)
        return json.dumps(declarations[ref])

    json_string_expanded = json_with_refs.sub(replace_reference, json_string)
    return json.loads(json_string_expanded)

def main():
    declarations_text = '''
        $obj1 = {"key": 10, "value": 12}
        $obj2 = {"superkey": $subobj}
        $subobj = {"subkey": 10}
    '''

    reference_json_text = '[$obj1, $obj2]'

    declarations_raw = parse_obj_declaration(declarations_text)
    declarations = {identifier: json_obj for identifier, _, json_obj in declarations_raw}

    expanded_json = parse_json_with_references(reference_json_text, declarations)
    print("Expanded JSON:", expanded_json)

main()