import parsy

node_parser = parsy.regex(r'[A-Za-z0-9_]+')

edge_parser = parsy.seq(node_parser << parsy.string('->'), node_parser)

graph_parser = parsy.seq(node_parser.sep_by(parsy.string(',')), parsy.string(';').optional(), edge_parser.sep_by(parsy.string(';')).optional())

class Graph:
    def __init__(self):
        self.nodes = []
        self.edges = []

    def add_node(self, node):
        self.nodes.append(node)

    def add_edge(self, from_node, to_node):
        self.edges.append((from_node, to_node))

    def adjacent(self, node):
        adj = []
        for edge in self.edges:
            if edge[0] == node:
                adj.append(edge[1])
        return adj

    def dfs(self, start):
        visited = set()
        self._dfs(start, visited)

    def _dfs(self, node, visited):
        visited.add(node)
        print(node)
        for neighbor in self.adjacent(node):
            if neighbor not in visited:
                self._dfs(neighbor, visited)

def parse_graph(input_string):
    nodes, _, edges = graph_parser.parse(input_string)
    g = Graph()
    for node in nodes:
        g.add_node(node)
    for edge in edges:
        g.add_edge(edge[0], edge[1])
    return g


input_string = 'A,B,C;A->B;B->C'
g = parse_graph(input_string)
g.dfs("A")
